# UN-Population
It converts raw open data, country and year wise population estimates, into charts. 
### Whats it Does

---

  - Plot the India population over years.
  - Plot Bar Chart of population of ASEAN countries For the year   2014.
  - Over the years, TOTAL population of SAARC countries.
  - Plot population of ASEAN  countries as groups over the years 2004 - 2014.
 
### Getting Started
 
---
#### Python Installation
- To install Python on linux run the following command.
```
sudo apt install python3-pip
```
- Once process is complete check the python Version
```
python3 -V
```
- To download python on windows [click here](https://www.python.org/downloads/windows/)

#### Virtual Environment
- To Install virtualenv using pip3 run the command:
```
sudo pip3 install virtualenv 
```
- To create a virtual environment,you can use any name insted of venv.
```
virtualenv venv 
```
- To Activate your virtual environment:
```
source venv/bin/activate 
```
- To deactivate run the command:
```
 deactivate
 ```

#### To Execute the program
  - To clone project copy and paste following link into terminal.
  ```python
     git clone git@gitlab.com:mountblue/cohort-17-python/hemant-patil/un-population.git
  ```
  - Download dataset from following link
    - [Click here to Download Dataset](https://datahub.io/core/population-growth-estimates-and-projections/r/population-estimates.csv)
    - Move the dataset to project directory.

  - Go to the Virtual Environment section and follow the steps
    - <a href="#Virtual Environment
">Install virtual environment </a>
    - <a href="#Virtual Environment
">create virtual environment </a> 
    - <a href="#Virtual Environment
">activate virtual environment </a> 
  - Install all the required packages.
  ```
   pip install -r requirements.txt 
  ```
  - To run the program
  ```
  python UN.py
  ```
 - Enter your choice.
 ```
  Enter your choice : 
1. India's population Vs years
2. South Asian Counties Population of 2014
3. SAARC Countries Population Vs year
4. Grouped bar graph of Asian Countries between year 2004-2014
5. Exit
 ```
- Get the Output

 
   
  
  
---

#### Output
   Click on output.png or 
   [click here](https://github.com/thehemantpatil/UNPopulation/blob/main/output.png)

<hr>

## <h2 align="center">Thanks</h2>



